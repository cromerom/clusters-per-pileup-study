import uproot
import mplhep as hep
import awkward as ak
import numpy as np
from scipy.optimize import curve_fit


import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib import cm
from math import factorial


from collections import defaultdict
# Set CMS Style
hep.style.use("CMS")

# Define the Gaussian function
def gaussian(x, mu, sigma, A):
    return A * np.exp(-(x - mu)**2 / (2 * sigma**2))

# Define the Poisson function
from scipy.special import gamma

def log_poisson(x, lamb):
    #return  np.log((lamb / gamma(x + 1) * np.exp(-lamb)))
    return x* np.log(lamb) -lamb - np.log(gamma(x + 1)) 

pileups = ['0.5', '1.0', '1.5', '2.0', '10.0', '30.0', '50.0', '75.0', '100.0', '120.0', '140.0', '160.0', '180.0', '200.0']
branches_to_load = ['cluster_nBits', 'disk', 'ring', 'detid', 'nClusters']

cmap = cm.get_cmap('viridis')  # Replace 'viridis' with any colormap you like
norm = plt.Normalize(vmin=0, vmax=len(pileups))

# Create a figure to hold all the histograms and curve fits
fig, ax = plt.subplots(figsize=(14, 12))

means = []
sigmas = []
integrals = []
gaussian_sigmas = []

counts_per_detid_for_pileup = []
means_per_detid_for_pileup = []
std_per_detid_for_pileup = []
mod_number = {}
means_pileups = {}
for i in range(14):

    pileup = pileups[i]
    print(f"PILEUP: {str(pileup)}")
    root_file = uproot.open(f"/afs/cern.ch/user/c/{USERNAME}/eos/itbrilclusterexporter/bril_output_PU_{pileup}_D91_cbarrera.root")
    tree = root_file["BRIL_IT_Analysis/Digis"]
    data = tree.arrays(branches_to_load)

    data_disk12 = data[data['disk'] == 12]
    data_disk12_ring1 = data_disk12[data_disk12['ring'] == 1]

    mod_number = {}
    mod_number2 = {}

    nClusters_per_entry = []

    means_detid = {}
    mod_number2_per_entry = []

    for entry in data_disk12_ring1:
        nClusters = sum(ak.flatten(entry['cluster_nBits']) >= 1)
        nClusters_per_entry.append(nClusters)
        #print(len(entry['detid']))
        mod_number2 = {}
        for t in range(len(entry['detid'])):
            detid = entry['detid'][t]
            if detid in mod_number2:
                mod_number2[detid] += entry['nClusters'][t]
            else:
                mod_number2[detid] = entry['nClusters'][t]
        
        mod_number2_per_entry.append(mod_number2)
    
    from collections import defaultdict
    import math

    counts_per_detid = defaultdict(int)
    means_per_detid = defaultdict(int)
    squared_diffs_per_detid = defaultdict(list)
    stddevs_per_detid = defaultdict(float)
    counts_per_detid2 = defaultdict(list)
    for d in mod_number2_per_entry:
        for key, value in d.items():
            counts_per_detid[key] += value
            counts_per_detid2[key].append(value)

    for key in counts_per_detid:
        means_per_detid[key] = counts_per_detid[key]/len(data_disk12_ring1)

    for d in mod_number2_per_entry:
        for key, value in d.items():
            squared_diff = (value - means_per_detid[key]) ** 2
            squared_diffs_per_detid[key].append(squared_diff)

    for key, values in squared_diffs_per_detid.items():
        variance = sum(values) / len(values)
        stddevs_per_detid[key] = math.sqrt(variance)

    
    # Convert back to regular dict
    counts_per_detid = dict(counts_per_detid)
    counts_per_detid2 = dict(counts_per_detid2)
    means_per_detid = dict(means_per_detid)
    stddevs_per_detid = dict(stddevs_per_detid)

    print("Checking mean counts")
    print("--------------------------")
    print(means_per_detid)

    means_per_detid_for_pileup.append(means_per_detid)
    std_per_detid_for_pileup.append(stddevs_per_detid)
    counts_per_detid_for_pileup.append(counts_per_detid2)

    
    nClusters_per_entry = np.array(nClusters_per_entry)

    counts, bin_edges = np.histogram(nClusters_per_entry, bins=10)
    bin_centers = (bin_edges[:-1] + bin_edges[1:]) / 2

    p0_poisson = [np.mean(nClusters_per_entry)]

    integral = np.sum(counts)
    integrals.append(integral)    

    p0_gaussian = [np.mean(nClusters_per_entry), np.std(nClusters_per_entry), np.max(counts)]

    
    if i < 4:
        params, _ = curve_fit(log_poisson, bin_centers, np.log(counts+0.00001), p0=p0_poisson)
    else:
        params, _ = curve_fit(gaussian, bin_centers, counts, p0=p0_gaussian)
        gaussian_sigmas.append(params[1])


    x_fit = np.linspace(min(bin_centers), max(bin_centers), 100)
    hep.histplot(counts, bin_edges, histtype='step', color='black', linestyle='dashed')
    color = cmap(norm(i))
    ax.plot(x_fit, gaussian(x_fit, *params) if i > 3 else log_poisson(x_fit, *params), label=f'PILEUP: {pileup}', color=color)
    
    print("Checking poisson")
    print("--------------------------")


    means.append(np.mean(nClusters_per_entry))
    sigmas.append(np.std(nClusters_per_entry))


hep.cms.label(data = False, label = '- Work in Progress', rlabel = '14 TeV')
#hep.cms.text(text = "Simulation", loc= 3)
print(means)
ax.set_xlim([0, 6000])
ax.set_ylim([0, 400])
ax.set_xlabel('Clusters per Entry', fontsize = 30)
ax.set_ylabel('Counts', fontsize = 30)
#ax.set_title('Number of clusters')
ax.legend()

# Save the figure
plt.savefig('/afs/cern.ch/user/c/{USERNAME}/eos/histos_per_module_per_event_gaussian.png', dpi = 300)

# Show the plot
plt.show()

# Convert pileups to int values
pileup_int = [float(p) for p in pileups]

counts_per_detid_per_pileup = defaultdict(list)
means_per_detid_per_pileup = defaultdict(list)
std_per_detid_per_pileup = defaultdict(list)

for pile in means_per_detid_for_pileup:
    for key, value in pile.items():
        means_per_detid_per_pileup[key].append(value)

for pile3 in counts_per_detid_for_pileup:
    for key3, value3 in pile3.items():
        counts_per_detid_per_pileup[key3].append(value3)

for pile2 in std_per_detid_for_pileup:
    for key2, value2 in pile2.items():
        std_per_detid_per_pileup[key2].append(value2)

actual_means = defaultdict(list)
actual_std = defaultdict(list)
for keym, valuem in counts_per_detid_per_pileup.items():
    for val in valuem:
        actual_means[keym].append(sum(val)/len(data_disk12_ring1))
        actual_std[keym].append(np.std(val))

for key in actual_means:
    actual_means[key][:4] = [i / 10 for i in actual_means[key][:4]]

for key in actual_means:
    integrals_m = []
    for p in counts_per_detid_per_pileup[key]:
        histo, bin_edges_m = np.histogram(p, bins=10)
        integral_m = np.sum(histo)
        integrals_m.append(integral_m)
    # Create a new figure
    values = actual_means[key]
    std = actual_std[key]
    fig2 = plt.figure(figsize=(14, 12))
    gs = gridspec.GridSpec(2, 1, height_ratios=[7, 3])  # 3:1 height ratio
    ax2 = plt.subplot(gs[0])
    ax3 = plt.subplot(gs[1], sharex=ax2)

    # Perform linear fit
    slope, intercept = np.polyfit(pileup_int[:4], values[:4], 1)


    # Calculate the fit values
    fit_values = slope * np.array(pileup_int) + intercept

    # Calculate the residuals
    residuals = (fit_values - values)
    abs_residuals = np.abs(residuals)

    # Calculate deviation
    #deviation = (residuals/ means) * 100
    deviation = residuals / values * 100

    # Plot the int values of pileups against the means with error bars
    ax2.errorbar(pileup_int, values, yerr=abs_residuals, marker='o', linestyle='None', color='blue', markerfacecolor='black', markeredgecolor='black')

    # Add CMS label
    plt.sca(ax2)  # Set the current axes to ax1
    hep.cms.label(data=False, label='- Work in Progress', rlabel='14 TeV')

    # Plot the linear fit
    ax2.plot(pileup_int, slope * np.array(pileup_int) + intercept, color='red')

    # Set the y-axis label for the first subplot
    ax2.set_ylabel('Mean cluster count per event')

    #Create Disk 4 Ring 1 annotation/textbox

    ax2.annotate('Disk 4 Ring 1', xy=(200, 5), xycoords='data',
                    xytext=(0.8, 0.3), textcoords='axes fraction',
                    arrowprops=dict(facecolor='white', edgecolor='none', shrink=0.01),
                    horizontalalignment='right', verticalalignment='top',
                    zorder=1e9, bbox=dict(facecolor='white', edgecolor='none', alpha=0.5))

    # Convert lists to numpy arrays
    sigmas_array = np.array(std)
    values_array = np.array(values)

    # Calculate the relative errors for the first 4 points
    relative_errors_poisson = np.sqrt(values_array[:4]) 


    # Calculate the relative errors for the rest of the points
    relative_errors = (np.array(sigmas_array) / np.sqrt(np.array(integrals_m)))

    # Plot the deviation against pileup_int with error bars
    ax3.errorbar(pileup_int, deviation, yerr= relative_errors, marker='None', linestyle='-', color='red', ecolor='black')

    # Set the x-axis label for the second subplot
    ax3.set_xlabel('<PU>')

    # Set the y-axis label for the second subplot
    ax3.set_ylabel('Residuals %')
    ax3.set_ylim([-5, 5])

    # Save the figure
    key_hex = np.base_repr(key, base=16)
    plt.savefig(f'detid_{key_hex}.png', dpi=300)
                        
    # Show the plot
    plt.show()
    plt.clf()



# Create a new figure
fig2 = plt.figure(figsize=(14, 12))
gs = gridspec.GridSpec(2, 1, height_ratios=[7, 3])  # 3:1 height ratio
ax2 = plt.subplot(gs[0])
ax3 = plt.subplot(gs[1], sharex=ax2)

# Perform linear fit
slope, intercept = np.polyfit(pileup_int[:3], means[:3], 1)
#slope, intercept = np.polyfit(pileup_int, means, 1)


# Calculate the fit values
fit_values = slope * np.array(pileup_int) + intercept

# Calculate the residuals
residuals = -(means - fit_values)
abs_residuals = np.abs(residuals)

# Calculate deviation
#deviation = (residuals/ means) * 100
deviation = residuals

# Plot the int values of pileups against the means with error bars
ax2.errorbar(pileup_int, means, yerr=abs_residuals, marker='o', linestyle='None', color='blue', markerfacecolor='black', markeredgecolor='black')

# Add CMS label
plt.sca(ax2)  # Set the current axes to ax1
hep.cms.label(data=False, label='- Work in Progress', rlabel='14 TeV')

# Plot the linear fit
ax2.plot(pileup_int, slope * np.array(pileup_int) + intercept, color='red')

# Set the y-axis label for the first subplot
ax2.set_ylabel('Mean cluster count per event')

#Create Disk 4 Ring 1 annotation/textbox

ax2.annotate('Disk 4 Ring 1', xy=(200, 5), xycoords='data',
                xytext=(0.8, 0.3), textcoords='axes fraction',
                arrowprops=dict(facecolor='white', edgecolor='none', shrink=0.01),
                horizontalalignment='right', verticalalignment='top',
                zorder=1e9, bbox=dict(facecolor='white', edgecolor='none', alpha=0.5))

# Convert lists to numpy arrays
sigmas_array = np.array(sigmas)
means_array = np.array(means)

# Calculate the relative errors for the first 4 points
relative_errors_poisson = np.sqrt(means_array[:4]) 

print("Going to print relative errors poisson")
print("--------------------------")
print(relative_errors_poisson)

# Calculate the relative errors for the rest of the points


relative_errors = np.array(sigmas_array) / np.sqrt(np.array(integrals))

# Plot the deviation against pileup_int with error bars
ax3.errorbar(pileup_int, deviation, yerr= relative_errors, marker='None', linestyle='-', color='red', ecolor='black')

# Set the x-axis label for the second subplot
ax3.set_xlabel('<PU>')

# Set the y-axis label for the second subplot
ax3.set_ylabel('Deviation')
ax3.set_ylim([-50, 50])

# Save the figure
plt.savefig('/afs/cern.ch/user/c/{USERNAME}/eos/tdr_per_module_per_event_gaussian_newCMSSW.png', dpi=300)
                     
# Show the plot
plt.show()

